package Test;


import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case of Stack.
 * @author Patinya Yongyai
 *
 */
public class StackTest {
	private Stack stack;
	
	@Before
	public void setUp() {
		StackFactory.setStackType(1);
	}
	
	@Test
	public void newStackIsEmpty(){
		stack = StackFactory.makeStack( 5 );
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	
	@Test( expected=java.util.EmptyStackException.class)
	public void testPopEmptyStack(){
		stack = StackFactory.makeStack( 5 );
		Assume.assumeTrue(stack.isEmpty());
		stack.pop();
		fail("No more element to pop");
	}
	
	@Test
	public void stackIsFull() {
		stack = StackFactory.makeStack( 2 );
		stack.push("one");
		assertFalse(stack.isFull());
		stack.push("two");
		assertTrue(stack.isFull());
	}
	
	@Test( expected=IllegalStateException.class)
	public void testEmptyPush() {
		stack = StackFactory.makeStack( 1 );
		stack.push("one");
		stack.push("two");
		fail("Stack is full");
	}
	
	@Test( expected=IllegalArgumentException.class)
	public void testObjectIsNull() {
		stack = StackFactory.makeStack( 1 );
		stack.push(null);
		assertNull(stack.peek());
		fail("No more element to peek");
	}
	
	@Test
	public void testNormalPush(){
		stack = StackFactory.makeStack(3);
		System.out.println(stack.size());
		stack.push("one");
		stack.push("two");
		stack.push("three");
		assertEquals(stack.size(), 3);		
	}
	
	@Test 
	public void testSize() {
		stack = StackFactory.makeStack( 5 );
		stack.push("one");
		assertEquals(stack.size(),1);
		stack.push("two");
		assertEquals(stack.size(), 2);
		stack.push("three");
		assertEquals(stack.size(), 3);
	}
	
	@Test
	public void testCapacity() {
		stack = StackFactory.makeStack(1);
		assertEquals(1, stack.capacity());
		stack = StackFactory.makeStack(0);
		assertEquals(0,stack.capacity());
	}
	
	@Test
	public void testNormalPeak(){
		stack = StackFactory.makeStack(3);
		stack.push("one");
		stack.push("two");
		stack.push("three");
		assertEquals("three",stack.peek());
		assertEquals("three",stack.peek());
		assertEquals("three",stack.peek());
	}
	
	@Test
	public void testEmptyPeak(){
		stack = StackFactory.makeStack(1);
		stack.peek();
		assertTrue(stack.isEmpty());
	}
	
	@Test (expected=java.lang.IllegalArgumentException.class)
	public void testNullPush(){
		stack = StackFactory.makeStack(1);
		stack.push(null);
	}
	
	@Test
	public void testNormalPop(){
		stack = StackFactory.makeStack(3);
		stack.push("one");
		stack.push("two");
		stack.push("three");
		assertEquals("three",stack.pop());
		assertEquals("two", stack.pop());
		assertEquals("one",stack.pop());
	}


}
